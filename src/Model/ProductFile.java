package Model;

import Ulity.Database;
import View.Display;
import View.HomeView;

import java.io.*;
import java.sql.Statement;
import java.util.ArrayList;

public class ProductFile {

    public static long startTime, endTime,takingTime;

    public static void savePref(long row){
        try {
            FileOutputStream fos = new FileOutputStream("utility.txt");
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(bos);
            Display.map.put("row",row);
            objectOutputStream.writeObject(Display.map);
            Display.numberOfPage = row;
            Display.page=1;
            Display.currentPage=0;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToFIle(){
        try {

            if(HomeView.isModify){
                System.out.println("Please wait.....");
                startTime=System.currentTimeMillis();
                Database.saveData();
                new File("recovery.txt").delete();
                HomeView.createFile = true;
                try {
                    Thread.sleep(200);
                    endTime=System.currentTimeMillis();
                    takingTime = endTime - startTime;
                    Display.displayMessage("Everything was saved! Taking time : " + takingTime);
                    HomeView.isModify=false;
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

            }else {
                Display.displayMessage("No sth change.");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeBackUpFile(String username,String database,String dbPass,String fileName){
        try {
            if(HomeView.products.size()!=0){
                File file=new File("File/");
                file.mkdirs();
                String os = System.getProperty("os.name").toLowerCase();
                if (os.contains("win")){
                    //Operating system is based on Windows
                    String executeCmd = "C:\\Program Files\\PostgreSQL\\"+HomeView.version+"\\bin\\pg_dump.exe --dbname=postgresql://"+username+":"+dbPass+"@localhost:5432/"+database+" > "+file.getAbsolutePath()+"/"+fileName+".dump";
                    Process p = Runtime.getRuntime().exec(executeCmd);
                    p.waitFor();
                }
                else if (os.contains("os")){
                    //Operating system is Apple OSX based
                    String executeCmd = "pg_dump --dbname=postgresql://"+username+":"+dbPass+"@localhost:5432/"+database+" > "+file.getAbsolutePath()+"/"+fileName+".dump";
                    Process p = Runtime.getRuntime().exec(new String[] { "/bin/sh"//$NON-NLS-1$
                            , "-c", executeCmd });
                    p.waitFor();
                }
                Display.displayMessage("Backup Successfully");

//                FileOutputStream fos = new FileOutputStream("File/"+HomeView.timeStamp+".dump");
//                BufferedOutputStream bos = new BufferedOutputStream(fos);
//                ObjectOutputStream objectOutputStream=new ObjectOutputStream(bos);
//                System.out.println("Please wait.....");
//                for (int i=0;i<HomeView.products.size();i++){
//                    objectOutputStream.writeUnshared(HomeView.products.get(i));
//                }
//                Thread.sleep(200);
//                objectOutputStream.close();
            }else {
                Display.displayMessage("No data to backup");
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void restoreFile(String fileName,String password,String databaseName,String tableName){
//        ObjectInputStream objectInputStream=null;
//        try {
//            FileInputStream fos = new FileInputStream(file);
//            BufferedInputStream bos = new BufferedInputStream(fos);
//            objectInputStream=new ObjectInputStream(bos);
//            System.out.println("Please wait.....");
//            HomeView.products.clear();
//            while (true){
//                Product product=(Product)objectInputStream.readUnshared();
//                HomeView.products.add(product);
//            }
//        }
//        catch (EOFException e){
//            try {
//                Thread.sleep(200);
//                System.out.println("Restore Successfully");
//                HomeView.restore=true;
//                HomeView.isModify = true;
//                try {
//                    objectInputStream.close();
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }
//            } catch (InterruptedException e1) {
//                e1.printStackTrace();
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
        try{
            Statement statement = Database.createConnection().createStatement();
            statement.execute("Drop table if exists "+tableName);
            Database.createConnection().close();
            File file = new File("");
            String executeCmd = "PGPASSWORD='"+password+"' psql -U "+databaseName+" -f "+file.getAbsolutePath()+"/"+fileName;
            Process p = Runtime.getRuntime().exec(new String[] { "/bin/sh", "-c", executeCmd });
            p.waitFor();

            System.out.println("Restore Successfully");
            Database.loadFromDB(HomeView.products);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
