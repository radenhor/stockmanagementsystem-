package Model;

import java.io.Serializable;

public class Product implements Serializable {
    int id; //for display in table
    String brand;
    float price;
    int quantity;
    String importedDate;
    int uid; // use for database only
    public Product(){
        uid = -1;
    }

    public Product(int id, String brand, float price, int quantity, String importedDate) {
        this();
        this.id = id;
        this.brand = brand;
        this.price = price;
        this.quantity = quantity;
        this.importedDate = importedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImportedDate() {
        return importedDate;
    }

    public void setImportedDate(String importedDate) {
        this.importedDate = importedDate;
    }

    public int getUID() {
        return uid;
    }

    public void setUID(int uid) {
        this.uid = uid;
    }
}
