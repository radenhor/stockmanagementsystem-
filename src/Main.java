import Ulity.Database;
import View.HomeView;

import java.io.IOException;
import java.sql.*;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        try {
            Database.registerDatabaseDriver();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                new HomeView().isSavedToFile();
                if(HomeView.fileWriter!=null) {
                    try {
                        HomeView.fileWriter.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        HomeView homeView = new HomeView();
        homeView.startUpView();
//        homeView.startProcess();
    }
}
