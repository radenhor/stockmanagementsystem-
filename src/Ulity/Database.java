package Ulity;

import Model.Product;
import View.HomeView;

import javax.xml.crypto.Data;
import java.sql.*;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;

public class Database {

    public static String URL = "jdbc:postgresql://localhost:5432/postgres";
    public static String USER = "postgres";
    public static String PASSWORD = "181099";
    public static int AUTO_ID;

    public static ArrayList<Product> insertTemp = new ArrayList<>();
    public static ArrayList<Product> deleteTemp = new ArrayList<>();
    public static ArrayList<Product> updateTemp = new ArrayList<>();

    //configure table here
    private static final String TABLE_PRODUCT_NAME = "tbproduct";
    //field name
    private static final String ID = "id";
    private static final String PRODUCT_NAME = "product_name";
    private static final String PRICE = "product_price"; // pls use numeric type in postgresql
    private static final String QUANTITY = "product_qty";
    private static final String IMPORT_DATE = "import_date"; // varchar
    private static final String IS_DELETE = "is_delete";

    public static void registerDatabaseDriver() throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
    }

    public static Connection createConnection() throws SQLException {
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }

    public static void insertALl() throws SQLException {
        String insertQuery = "INSERT INTO " +
                TABLE_PRODUCT_NAME + " (" + PRODUCT_NAME + ", " + PRICE + ", " + QUANTITY + ", " + IMPORT_DATE + ", " + IS_DELETE + ")" +
                "VALUES (?, ?, ?, ?, ?);";
        try(Connection con = createConnection()) {
            PreparedStatement preparedStatement = con.prepareStatement(insertQuery);
            insertTemp.forEach( product -> {
                try {
                    LocalDate localDate = LocalDate.now();
                    preparedStatement.setString(1, product.getBrand());
                    preparedStatement.setDouble(2, product.getPrice());
                    preparedStatement.setInt(3, product.getQuantity());
//                    preparedStatement.setString(4,"@"+product.getImportedDate());
                    preparedStatement.setObject(4,localDate);
                    preparedStatement.setBoolean(5, false);
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            insertTemp.clear();
        }
    }

    public static void updateAll() throws SQLException {
        String updateQuery = "UPDATE " +
                TABLE_PRODUCT_NAME + " SET " + PRODUCT_NAME + " = ?," + PRICE +" =?," + QUANTITY + "=? WHERE " + ID + " = ?;";
        try(Connection con = createConnection()) {
            PreparedStatement preparedStatement = con.prepareStatement(updateQuery);
            updateTemp.forEach( product -> {
                try {
                    preparedStatement.setString(1, product.getBrand());
                    preparedStatement.setDouble(2, product.getPrice());
                    preparedStatement.setInt(3, product.getQuantity());
                    preparedStatement.setInt(4, product.getUID());
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            updateTemp.clear();
        }
    }

    public static void removeAll() throws SQLException {
        String updateQuery = "UPDATE " + TABLE_PRODUCT_NAME + " SET " + IS_DELETE + "= true WHERE " + ID + " = ?";
        try(Connection con = createConnection()) {
            PreparedStatement preparedStatement = con.prepareStatement(updateQuery);
            deleteTemp.forEach( product -> {
                try {
                    preparedStatement.setInt(1, product.getUID());
                    preparedStatement.executeUpdate();
                    insertTemp.remove(product);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }
        deleteTemp.clear();
    }

    public static void saveData() {
        try {
            insertALl();
            updateAll();
            removeAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getLastID() throws SQLException {

        try(Connection con = createConnection()) {
            String selectQuery = "SELECT " + ID + " FROM " + TABLE_PRODUCT_NAME +  " ORDER BY " + ID + " DESC LIMIT 1";
            ResultSet resultSet = con.createStatement().executeQuery(selectQuery);
            while (resultSet.next()) {
                return resultSet.getInt(Database.ID);
            }
        }
        return - 1;
    }

    public static void loadFromDB(List<Product> productsList) throws SQLException {
        if (productsList == null)
            throw new NullPointerException("productList can not be null");
        try(Connection con = createConnection()) {
            String selectQuery = "SELECT * FROM " + TABLE_PRODUCT_NAME + " WHERE " + IS_DELETE + " = false ORDER BY " + ID + " ASC;";
            ResultSet resultSet = con.createStatement().executeQuery(selectQuery);
            HomeView.products.clear();
            while (resultSet.next()) {
                int id = resultSet.getInt(Database.ID);
                String productName = resultSet.getString(Database.PRODUCT_NAME);
                double price = resultSet.getDouble(Database.PRICE);
                int quantity = resultSet.getInt(Database.QUANTITY);
                String importDate = resultSet.getString(Database.IMPORT_DATE);
                Product product = new Product(id, productName, (float)price, quantity, importDate);
                product.setUID(id);
                HomeView.products.add(product);
            }

        }

    }

}
