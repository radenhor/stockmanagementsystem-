package Ulity;

import View.Display;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public static boolean isNumber(String num,String name){
        try{
            long n = Long.valueOf(num.trim());
            if(n>0){
                return true;
            } else {
                Display.displayMessage(name+" must be greater than 0.");
                return false;
            }

        }catch (Exception e){
            Display.displayMessage("Invalid "+name+" ...!");
            return false;
        }
    }

    public static boolean isFloat(String num,String name){
        try{
            float n = Float.valueOf(num.trim());
            if(n>0){
                return true;
            } else {
                Display.displayMessage(name+" must be greater than 0.");
                return false;
            }

        }catch (Exception e){
            Display.displayMessage("Invalid "+name+" ...!");
            return false;
        }
    }

    public static boolean isValidName(String name){
        Pattern pattern =  Pattern.compile("[a-zA-Z\\s]{2,15}");
        Matcher matcher = pattern.matcher(name);
        if(matcher.matches()){
            return true;
        }
        else{
            if(!name.matches("[a-zA-Z]{1,15}")){
                Display.displayMessage("Name is not valid!");
            }else if(name.length()>15){
                Display.displayMessage("Name must be less than 15 characters.");
            }else if(name.length()<2) {
               Display.displayMessage("Name must be more than 1 characters.");
            }
        }

        return false;

    }

    public static boolean isNumber(String num,String name,int l){
        try{
            long n = Long.valueOf(num.trim());
            if(n>0){
                if(n>l){
                    Display.displayMessage("Invalid "+name+" !");
                    return false;
                }
                return true;
            } else {
                Display.displayMessage(name+" must be greater than 0.");
                return false;
            }

        }catch (Exception e){
            Display.displayMessage("Invalid "+name+" !");
            return false;
        }
    }


}
