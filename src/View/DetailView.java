package View;

import Controller.StockController;
import Model.Product;
import Ulity.Validator;

import java.util.Scanner;

public class DetailView {

    public static void detailView(){
        Scanner sc = new Scanner(System.in);
        String id;
        do{
            System.out.println("input product id :");
            id = sc.nextLine();
        }while (!Validator.isNumber(id,"ID"));

        Product product = new StockController().display(Long.valueOf(id));
        if (product != null){
            Display.comfirmBox(product);
        }else{
            Display.displayMessage("Wrong ID");
        }

    }
}
