package View;


import Controller.StockController;
import Model.Product;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import org.omg.PortableInterceptor.HOLDING;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Display {

    public static HashMap<String,Object> map=new HashMap<>();
    public static long currentPage = 0,page=1,numberOfPage=1,totalPage=(HomeView.products.size()/numberOfPage)+(HomeView.products.size()%numberOfPage<numberOfPage&&HomeView.products.size()%numberOfPage!=0?1:0);

    public static void comfirmBox(Product product){
        Table table=new Table(1, BorderStyle.UNICODE_DOUBLE_BOX,new ShownBorders(false,false,false,false,false,false,true,true,true,true),false,10);
        table.setColumnWidth(0, 90, 100);
        table.addCell(" ID             :" + product.getId(),new CellStyle(CellStyle.HorizontalAlign.left));
        table.addCell(" Brand          :" + product.getBrand(),new CellStyle(CellStyle.HorizontalAlign.left));
        table.addCell(" Quantity       :" + product.getQuantity(),new CellStyle(CellStyle.HorizontalAlign.left));
        table.addCell(" Price          :" + product.getPrice(),new CellStyle(CellStyle.HorizontalAlign.left));
        table.addCell(" Imported Date  :" + product.getImportedDate(),new CellStyle(CellStyle.HorizontalAlign.left));
        System.out.println(table.render());
    }


    public static void displayMenu(){
        Table table=new Table(1, BorderStyle.UNICODE_DOUBLE_BOX,new ShownBorders(false,false,false,false,false,false,true,true,true,true),false,10);
        table.setColumnWidth(0, 104, 100);
        table.addCell(" *)Display | W)rite | R)ead | U)pdate | D)elete | F)irst | P)revious | N)ext | L)ast",new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("S)earch | G)oto | Se)t row  | Sa)ve | Ba)ckup | Re)store | H)elp | E)ixt",new CellStyle(CellStyle.HorizontalAlign.center));
        System.out.println(table.render());
    }

    public static void displayData(ArrayList<Product> products){
        numberOfPage = map.get("row")==null?1:(long)map.get("row");

        totalPage=(products.size()/numberOfPage)+((products.size()%numberOfPage<numberOfPage)&&(products.size()%numberOfPage!=0)?1:0);

        Table table=new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER,new ShownBorders(true,true,true,true,true,true,true,true,true,true),false,10);

        if(HomeView.products.size()!=0){
            table.addCell("ID",new CellStyle(CellStyle.HorizontalAlign.center));
            table.addCell("BRAND",new CellStyle(CellStyle.HorizontalAlign.center));
            table.addCell("QUANTITY",new CellStyle(CellStyle.HorizontalAlign.center));
            table.addCell("PRICE",new CellStyle(CellStyle.HorizontalAlign.center));
            table.addCell("DATE",new CellStyle(CellStyle.HorizontalAlign.center));
            for(long i=currentPage;i<currentPage+numberOfPage;i++){
                if(i>=products.size())break;
                table.addCell(""+ products.get((int)i).getId(),new CellStyle(CellStyle.HorizontalAlign.center));
                table.addCell(" "+products.get((int)i).getBrand());
                table.addCell(" "+products.get((int)i).getQuantity(),new CellStyle(CellStyle.HorizontalAlign.center));
                table.addCell(" "+products.get((int)i).getPrice(),new CellStyle(CellStyle.HorizontalAlign.center));
                table.addCell(" "+products.get((int)i).getImportedDate());
            }

            table.setColumnWidth(0, 20, 20);
            table.setColumnWidth(1, 20, 20);
            table.setColumnWidth(2, 20, 20);
            table.setColumnWidth(3, 20, 20);
            table.setColumnWidth(4, 20, ("Total Record : "+products.size()).length());

            table.addCell("   Pre "+page+" / "+(totalPage)+" Next",4);
            table.addCell("Total Record : "+products.size(),new CellStyle(CellStyle.HorizontalAlign.left));

        }else {
            Display.displayMessage("Product is empty");
        }

        System.out.println(table.render());
    }

    public static void displayMessage(String msg){
        Table table=new Table(1, BorderStyle.DESIGN_PAPYRUS,new ShownBorders(true,true,true,true,true,true,true,true,false,false),false,10);
        table.setColumnWidth(0, 40, msg.length());
        table.addCell(msg,new CellStyle(CellStyle.HorizontalAlign.center));
        System.out.println(table.render());
    }
    public static void searchResult(){

        Table table=new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER,new ShownBorders(true,true,true,true,true,true,true,true,true,true),false,10);
        table.setColumnWidth(0, 20, 20);
        table.setColumnWidth(1, 20, 20);
        table.setColumnWidth(2, 20, 20);
        table.setColumnWidth(3, 20, 20);
        table.setColumnWidth(4, 20, 20);

        table.addCell("ID",new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("BRAND",new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("QUANTITY",new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("PRICE",new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("DATE",new CellStyle(CellStyle.HorizontalAlign.center));

        if(StockController.searchProduct.size()!=0) {
            StockController.totalPage = (StockController.searchProduct.size()/Display.numberOfPage)+((StockController.searchProduct.size()%Display.numberOfPage<Display.numberOfPage)&&(StockController.searchProduct.size()%Display.numberOfPage!=0)?1:0);
            for (long i = StockController.currentPage; i < StockController.currentPage + Display.numberOfPage; i++) {
                if (i >= StockController.searchProduct.size()) break;
                table.addCell("" + StockController.searchProduct.get((int) i).getId(), new CellStyle(CellStyle.HorizontalAlign.center));
                table.addCell(" " + StockController.searchProduct.get((int) i).getBrand());
                table.addCell(" " + StockController.searchProduct.get((int) i).getQuantity(), new CellStyle(CellStyle.HorizontalAlign.center));
                table.addCell(" " + StockController.searchProduct.get((int) i).getPrice(), new CellStyle(CellStyle.HorizontalAlign.center));
                table.addCell(" " + StockController.searchProduct.get((int) i).getImportedDate());
            }
            table.addCell("   Page "+StockController.page+" of "+(StockController.totalPage),3);
            table.addCell("Total Record : "+StockController.searchProduct.size(),new CellStyle(CellStyle.HorizontalAlign.center),2);
            System.out.println(table.render());
        }
    }

    public static void displayHelpForm(){
            Table table = new Table(1, BorderStyle.CLASSIC_COMPATIBLE,new ShownBorders(false,false,false,false,false,false,true,true,true,true),false,10);
            table.setColumnWidth(0,80,90);
            table.addCell(" 1.   Press   *  : Display all record of products");
            table.addCell(" 2.   Press   w  : Add new product");
            table.addCell("      Press   #w[pro_name:unit_price:qty] : shortcut for add new product");
            table.addCell(" 3.   Press   r  : read Content any content");
            table.addCell("      Press   #r[proId] : shortcut for read product by Id");
            table.addCell(" 4.   Press   u  : Update Data");
            table.addCell(" 5.   Press   d  : Delete Data");
            table.addCell("      Press   #d[proId] : shortcut for delete product by Id");
            table.addCell(" 6.   Press   f : Display First Page");
            table.addCell(" 7.   Press   p : Display Previous Page");
            table.addCell(" 8.   Press   n : Display Next Page");
            table.addCell(" 9.   Press   l : Display last Page");
            table.addCell(" 10.  Press   s : Search product by name ");
            table.addCell("      Press   #s[proName] : shortcut for search by name");
            table.addCell(" 11.  Press   sa : Save record to file");
            table.addCell(" 12.  Press   ba : Backup data");
            table.addCell(" 13.  Press   re : Restore data ");
            table.addCell(" 14.  Press   h : Help");
            table.addCell(" 15.  Press   se : Set number of row to display in a page");
            table.addCell("      Press   #sr[numberOfRow] : shortcut for set number of row");
            table.addCell(" 16.  Press   g : Goto specific page");
            table.addCell(" 17.  Press   e : Exit the program");

        System.out.println(table.render());
    }
}
