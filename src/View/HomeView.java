package View;

import Controller.*;
import Model.Product;
import Model.ProductFile;
import Ulity.Database;
import Ulity.Validator;
import com.github.lalyos.jfiglet.FigletFont;
import java.awt.*;
import java.io.*;
import java.math.BigDecimal;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;

public class HomeView extends Component{

    public static int version;
    public static boolean isModify = false;
    private Scanner sc = new Scanner(System.in);
    public static boolean isRead=true;
    public static boolean paginationForSearch = false;
    public static ArrayList<Product> products=new ArrayList<>();
    public static String timeStamp = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(Calendar.getInstance().getTime());
    public static FileWriter fileWriter = null;
    public static boolean createFile = true;
    static {
        try{
            try{
                try {
                    DatabaseMetaData metaData = Database.createConnection().getMetaData();
                    version = metaData.getDatabaseMajorVersion();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                ObjectInputStream objectInputStream=new ObjectInputStream(new FileInputStream("utility.txt"));
                Display.map = (HashMap<String, Object>) objectInputStream.readObject();
//                System.out.println(Display.map);
                if(Display.map.get("row")!=null)
                    Display.numberOfPage = (long)Display.map.get("row");
//                if(Display.map.get("page")!=null)
//                    Display.page = (long)Display.map.get("page");
//                else
//                    Display.page=1;
//                if(Display.map.get("currentPage")!=null)
//                    Display.currentPage = (long)Display.map.get("currentPage");
//                else
//                    Display.currentPage=0;
                Display.page=1;
                Display.currentPage=0;
                Display.totalPage=(HomeView.products.size()/Display.numberOfPage)+((HomeView.products.size()%Display.numberOfPage<Display.numberOfPage)&&(HomeView.products.size()%Display.numberOfPage!=0)?1:0);

                objectInputStream.close();

            }catch (EOFException e){
                e.printStackTrace();
            }
            catch (FileNotFoundException e){
                Display.numberOfPage = 1;
           }catch (Exception e){
                e.printStackTrace();
            }
            File file = new File("recovery.txt");
            if(file.exists()){
                    do{
                        System.out.println("You forgot to save sth do want to recovery Yes[Y] or No[o]?");
                        String ch = new Scanner(System.in).nextLine();
                        if(ch.toLowerCase().equals("y")){

                            BufferedReader fileReader = new BufferedReader(new FileReader("recovery.txt"));
                            try {
                                Statement statement = Database.createConnection().createStatement();
                                while (fileReader.ready()){
                                    String data = fileReader.readLine();
                                    if(data.split("#")[1].equals("insert")){
                                        String name = data.split("#")[0].split("&")[0];
                                        int qty = Integer.valueOf(data.split("#")[0].split("&")[1]);
                                        BigDecimal price = BigDecimal.valueOf(Double.valueOf(data.split("#")[0].split("&")[2]));
                                        String insert = "Insert into tbproduct (product_name,product_qty,product_price,import_date) values('"+name+"',"+qty+","+price+",'2019-05-06')";
                                        statement.executeUpdate(insert);
                                    }else if(data.split("#")[1].equals("updateall")){
                                        String update = "UPDATE tbproduct set product_name = '"+data.split("#")[0].split("&")[0]+"', product_qty = "+data.split("#")[0].split("&")[1]+", product_price = "+data.split("#")[0].split("&")[2]+", import_date = '2019-05-06' where id = "+data.split("#")[0].split("&")[4];
                                        statement.executeUpdate(update);
                                    }else if(data.split("#")[1].equals("updateqty")){
                                        String update = "UPDATE tbproduct set product_qty = " + data.split("#")[0].split("&")[0] + "where id = "+data.split("#")[0].split("&")[1];
                                        statement.executeUpdate(update);
                                    }else if(data.split("#")[1].equals("updateprice")){
                                        String update = "UPDATE tbproduct set product_price = " + data.split("#")[0].split("&")[0] + "where id = "+data.split("#")[0].split("&")[1];
                                    } else if(data.split("#")[1].equals("delete")){
                                        String delete = "update tbproduct set is_delete = true where id = "+data.split("#")[0];
                                        statement.executeUpdate(delete);
                                    }
                                }
                                file.delete();
                                break;
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }

                        }else if(ch.toLowerCase().equals("n")) {
                            file.delete();
                            break;
                        }else {
                            Display.displayMessage("Invalid");
                        }
                    }while (true);
                }


//            ObjectInputStream productFile=new ObjectInputStream(new FileInputStream("product.txt"));
//            productFile.close();
        }
        catch (Exception e){
        }
    }

    public void startProcess(){



        do {
            drawMain();
            System.out.print("command --> ");
            String choice = sc.nextLine();
            choice = choice.toLowerCase();
            if(choice.startsWith("#")){
                if(choice.toLowerCase().equals("#w10m")){
                    String date = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
                    Product product = new Product(HomeView.products.size(),"java",2000,10,date);
                    new StockController().insertBigData(product);
                    ProductFile.writeToFIle();
                } else if(choice.startsWith("#r")){
                    choice = choice.toLowerCase();
                    try{
                        if(choice.split("r").length==2){
                            long id = Integer.valueOf(choice.split("r")[1].trim());
                            System.out.println(id);
                            if(new StockController().display(id)==null){
                                Display.displayMessage("Not Found");
                            }else {
                                Display.comfirmBox(new StockController().display(id));
                            }
                        }else {
                            Display.displayMessage("Invalid");
                        }


                    }catch (NumberFormatException e){
                        Display.displayMessage("Invalid");
                    }
                }else if(choice.toLowerCase().startsWith("#w")){
                    choice = choice.toLowerCase();
                    //#wjava:1000:20
                    if(choice.trim().length()>2){
                        if(choice.charAt(2)==':'){
                            Display.displayMessage("Invalid");
                        }else {
                            if(choice.split(":").length==3){
                                if (choice.split(":")[2].split(">").length==1){
                                    choice=choice.replaceAll("#w","");
                                    String brand = choice.split(":")[0];
                                    try{
                                        if(Validator.isValidName(brand)&&Validator.isNumber(choice.split(":")[1],"Price")&&Validator.isNumber(choice.split(":")[2],"Qty")){
                                            float price = Float.valueOf(choice.split(":")[1]);
                                            int qty = Integer.valueOf(choice.split(":")[2]);
                                            String date = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
                                            if(new StockController().insert(new Product((products.size()==0)?1:products.size()+1,brand,price,qty,date))){
                                                Display.displayMessage("Insert success!");
                                                isModify = true;
                                                Display.totalPage=(HomeView.products.size()/Display.numberOfPage)+((HomeView.products.size()%Display.numberOfPage<Display.numberOfPage)&&(HomeView.products.size()%Display.numberOfPage!=0)?1:0);
                                                Display.currentPage = HomeView.products.size() - (HomeView.products.size()%Display.numberOfPage==0?Display.numberOfPage:HomeView.products.size()%Display.numberOfPage);
                                                Display.page=Display.totalPage;
                                            }
                                        }

                                    }catch (NumberFormatException e){
                                        Display.displayMessage("Invalid");
                                    }
                                }else{
                                    //#wjava:1000:20>10
                                    String brand = choice.split(":")[0].split("w")[1];
                                    String numOfRecord = choice.split(":")[2];
                                    if (numOfRecord.split(">").length==2){
                                        try{
                                            float price = Float.valueOf(choice.split(":")[1]);
                                            int qty = Integer.valueOf(numOfRecord.split(">")[0]);
                                            String date = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
                                            long records = Long.valueOf(numOfRecord.split(">")[1]);
                                            if (new StockController().insertMultiRecord(new Product((products.size()==0)?1:products.size()+1,brand,price,qty,date),records)){
                                                Display.displayMessage("Insert success!");
                                                isModify = true;
                                                Display.totalPage=(HomeView.products.size()/Display.numberOfPage)+((HomeView.products.size()%Display.numberOfPage<Display.numberOfPage)&&(HomeView.products.size()%Display.numberOfPage!=0)?1:0);
                                                Display.currentPage = HomeView.products.size() - (HomeView.products.size()%Display.numberOfPage==0?Display.numberOfPage:HomeView.products.size()%Display.numberOfPage);
                                                Display.page=Display.totalPage;
                                            }
                                        }catch (NumberFormatException e){
                                            Display.displayMessage("Invalid command");
                                        }

                                    }else{
                                        Display.displayMessage("Invalid command");
                                    }
                                }

                            }else{
                                Display.displayMessage("Invalid command");
                            }
                        }
                    }



                }else if(choice.startsWith("#d")){
                    choice = choice.toLowerCase();
                    try{
                        if(choice.split("d").length==2){
                            long id = Integer.valueOf(choice.split("d")[1].trim());
                            if(products.size()!=0){
                                if(new StockController().remove(id)){
                                    Display.displayMessage("Delete success!");
                                    if(Display.page>(products.size()/Display.numberOfPage)+((products.size()%Display.numberOfPage<Display.numberOfPage)&&(products.size()%Display.numberOfPage!=0)?1:0)){
                                        Display.page--;
                                        Display.currentPage-=Display.numberOfPage;
                                    }
                                    isModify = true;
                                }else {
                                    Display.displayMessage("Wrong id");
                                }

                            }else {
                                Display.displayMessage("Product is empty");
                            }
                        }else {
                            Display.displayMessage("Invalid");
                        }



                    }catch (NumberFormatException e){
                        Display.displayMessage("Invalid");
                    }


                }
//                else if(choice.startsWith("#s")){
//                    choice = choice.replaceAll("#s","");
//                        if(choice!=""){
//                            paginationForSearch = true;
//                            String name = choice.trim();
//                            StockController stockController = new StockController();
//                            if (stockController.searchByName(name)){
//                                Display.searchResult();
//                            }else{
//                                Display.displayMessage("Search not found");
//                            }
//                        }else {
//                            Display.displayMessage("Invalid");
//                        }
//
//                }
                else if(choice.startsWith("#s")){
                    choice = choice.toLowerCase();
                    try{
                        Integer.valueOf(choice.replaceAll("#sr",""));
                            if(Validator.isNumber(choice=choice.replaceAll("#sr",""),"Number of Row",100)){
                                long row = Integer.valueOf(choice.trim());
                                Display.map.put("row",row);
                                Display.numberOfPage = row;
                                Display.page=1;
                                Display.currentPage=0;
                                Display.displayMessage("Number of row has set to "+Display.numberOfPage);
                            }
                    }catch (Exception e){
                        choice = choice.replaceAll("#s","");
                        if(choice!=""){
                            paginationForSearch = true;
                            String name = choice.trim();
                            StockController stockController = new StockController();
                            if (stockController.searchByName(name)){
                                Display.searchResult();
                            }else{
                                Display.displayMessage("Search not found");
                            }
                        }else {
                            Display.displayMessage("Invalid");
                        }
                    }




                }
                else if(choice.equals("#h")){
                    Display.displayHelpForm();
                }
            }else {
                switch (choice.toLowerCase().trim()) {
                    case "w": {
                        InsertView.insert();
                        break;
                    }
                    case "h": {
                        Display.displayHelpForm();
                        break;
                    }
                    case "sa": {
                        ProductFile.writeToFIle();
                        break;
                    }

                    case "*": {
                        paginationForSearch = false;
                        Display.displayData(HomeView.products);
                        break;
                    }
                    case "r": {
                        DetailView.detailView();
                        break;
                    }
                    case "u": {
                        UpdateView.update();
                        break;
                    }
                    case "d": {
                        RemoveView.removeByID();
                        break;
                    }
                    case "s": {
                        paginationForSearch = true;
                        SearchView.searchByName();
                        break;
                    }
                    case "n":{
                        if(HomeView.paginationForSearch){
                            StockController.currentPage+=Display.numberOfPage;
                            StockController.page+=1;
                            if(StockController.page > StockController.totalPage){
                                StockController.currentPage=0;
                                StockController.page=1;
                            }
                            Display.searchResult();

                        }else {
                            Display.totalPage=(products.size()/Display.numberOfPage)+((products.size()%Display.numberOfPage<Display.numberOfPage)&&(products.size()%Display.numberOfPage!=0)?1:0);
                            Display.currentPage+=Display.numberOfPage;
                            Display.page+=1;
                            if(Display.page > Display.totalPage){
                                Display.currentPage=0;
                                Display.page=1;
                            }
//                            if(!isModify){
//                                Display.map.put("page",Display.page);
//                                Display.map.put("currentPage",Display.currentPage);
//                            }
                            Display.displayData(HomeView.products);
                        }
                        break;
                    }
                    case "p":{
                        if(HomeView.paginationForSearch){
                            StockController.currentPage-=Display.numberOfPage;
                            StockController.page-=1;
                            if(StockController.page<1){
                                if(StockController.currentPage<=0){
                                    StockController.currentPage = StockController.searchProduct.size() - (StockController.searchProduct.size()%Display.numberOfPage==0?Display.numberOfPage:StockController.searchProduct.size()%Display.numberOfPage);
                                }

                                StockController.page=StockController.totalPage;

                            }
                            Display.searchResult();
                        }else {
                            Display.currentPage-=Display.numberOfPage;
                            Display.page-=1;
                            Display.totalPage=(products.size()/Display.numberOfPage)+((products.size()%Display.numberOfPage<Display.numberOfPage)&&(products.size()%Display.numberOfPage!=0)?1:0);
                            if(Display.page<1){

                                if(Display.currentPage<=0){

                                    Display.currentPage = products.size() - (products.size()%Display.numberOfPage==0?Display.numberOfPage:products.size()%Display.numberOfPage);

                                }

                                Display.page=Display.totalPage;
                            }
//                            if(!isModify){
//                                Display.map.put("page",Display.page);
//                                Display.map.put("currentPage",Display.currentPage);
//                            }
                            Display.displayData(HomeView.products);
                        }


                        break;
                    }
                    case "l":{
                        if(HomeView.paginationForSearch){
//                            StockController.currentPage = StockController.searchProduct.size() - (StockController.searchProduct.size()%Display.numberOfPage==0?Display.numberOfPage:StockController.searchProduct.size()%Display.numberOfPage);
                            StockController.currentPage = StockController.searchProduct.size() - (StockController.searchProduct.size()%Display.numberOfPage==0?Display.numberOfPage:StockController.searchProduct.size()%Display.numberOfPage);
                            StockController.page=StockController.totalPage;
                            Display.searchResult();
                        }else {
                            Display.currentPage = products.size() - (products.size()%Display.numberOfPage==0?Display.numberOfPage:products.size()%Display.numberOfPage);
                            Display.totalPage=(products.size()/Display.numberOfPage)+((products.size()%Display.numberOfPage<Display.numberOfPage)&&(products.size()%Display.numberOfPage!=0)?1:0);
                            Display.page=Display.totalPage;
//                            if(!isModify){
//                                Display.map.put("page",Display.page);
//                                Display.map.put("currentPage",Display.currentPage);
//                            }
                            Display.displayData(HomeView.products);
                        }
                        break;
                    }
                    case "f":{
                        if(HomeView.paginationForSearch){
                            StockController.currentPage=0;
                            StockController.page=1;
                            Display.searchResult();
                        }else {
                            Display.currentPage=0;
                            Display.page=1;
//                            if(!isModify){
//                                Display.map.put("page",Display.page);
//                                Display.map.put("currentPage",Display.currentPage);
//                            }
                            Display.displayData(HomeView.products);
                        }

                        break;
                    }

                    case "e": {

                        beforeExit();
                        Display.displayMessage("Good bye 👋");
                        isSavedToFile();
                        System.exit(0);

                    }
                    case "g":{
                        Display.totalPage=(products.size()/Display.numberOfPage)+((products.size()%Display.numberOfPage<Display.numberOfPage)&&(products.size()%Display.numberOfPage!=0)?1:0);
                        GotoPage.goToPage();
                        break;
                    }

                    case "se":{
                        Scanner sc=new Scanner(System.in);
                        String row;
                        do{
                            System.out.println("Set Row:");
                            row = sc.nextLine();
                        }while (!Validator.isNumber(row,"Row",101));
                        Display.displayMessage("Number of row has set to "+row);
                        ProductFile.savePref(Long.valueOf(row));
                        break;
                    }
                    case "ba":{
                        ProductFile.writeBackUpFile(Database.USER,"postgres",Database.PASSWORD,timeStamp);
                        break;
                    }

                    case "re":{

                        Restore.restore();

                        break;
                    }
                }
            }
//            isSavedToFile();
        }while (true);
    }
// Check user has saved
    public void isSavedToFile() {
        if(isModify){
            try {
                FileOutputStream fos = new FileOutputStream("product_recovery.txt");
                BufferedOutputStream bos = new BufferedOutputStream(fos);
                ObjectOutputStream objectOutputStream=new ObjectOutputStream(bos);
                Display.map.put("save",true);
                for(Product product:HomeView.products){
                    objectOutputStream.writeUnshared(product);
                }
                objectOutputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            Display.map.put("save",false);
        }
        try {
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(new FileOutputStream("utility.txt"));
            objectOutputStream.writeUnshared(Display.map);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void drawMain() {
        Display.displayMenu();
    }

    public void startUpView() throws InterruptedException{
        // using font font2.flf, located somewhere in classpath under path /flf/font2.flf
        String asciiArt2 = null;
        try {
            int delay=300;
            asciiArt2 = FigletFont.convertOneLine(FigletFont.class.getResourceAsStream("/standard.flf"), "Stock Management");
            System.out.println("                     ____    ____        ____    ____    _____   __  __  ____           __     ");
            Thread.sleep(delay);
            System.out.println("                    /\\  _`\\ /\\  _`\\     /\\  _`\\ /\\  _`\\ /\\  __`\\/\\ \\/\\ \\/\\  _`\\       /'__`\\   ");
            Thread.sleep(delay);
            System.out.println("                    \\ \\,\\L\\_\\ \\ \\L\\ \\   \\ \\ \\L\\_\\ \\ \\L\\ \\ \\ \\/\\ \\ \\ \\ \\ \\ \\ \\L\\ \\    /\\_\\L\\ \\  ");
            Thread.sleep(delay);
            System.out.println("                     \\/_\\__ \\\\ \\ ,  /    \\ \\ \\L_L\\ \\ ,  /\\ \\ \\ \\ \\ \\ \\ \\ \\ \\ ,__/    \\/_/_\\_<_ ");
            Thread.sleep(delay);
            System.out.println("                       /\\ \\L\\ \\ \\ \\\\ \\    \\ \\ \\/, \\ \\ \\\\ \\\\ \\ \\_\\ \\ \\ \\_\\ \\ \\ \\/       /\\ \\L\\ \\");
            Thread.sleep(delay);
            System.out.println("                       \\ `\\____\\ \\_\\ \\_\\   \\ \\____/\\ \\_\\ \\_\\ \\_____\\ \\_____\\ \\_\\       \\ \\____/");
            Thread.sleep(delay);
            System.out.println("                        \\/_____/\\/_/\\/ /    \\/___/  \\/_/\\/ /\\/_____/\\/_____/\\/_/        \\/___/ ");

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(asciiArt2);
//        isSavedToFile();

        loadingFromFile();
        

    }

     synchronized void loading(){
        String loading = "Please wait .....";

            try {
                for(int i=0;i<loading.length();i++){
                    System.out.print(loading.charAt(i));
                    Thread.sleep(50);
                    if(i==11){
                        wait();
                    }else if(i%2==0){
                        Thread.sleep(10);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

    }

     synchronized void loadFile(){
         long startTime = System.currentTimeMillis();
         try {
             if(isRead){
                 Database.loadFromDB(HomeView.products);

             }
             loadingTime(startTime);

         } catch (SQLException e) {
             e.printStackTrace();
         }

     }

    private void loadingFromFile() {

            new Thread(()->{
                loading();
            }).start();

            new Thread(()->{
                loadFile();
            }).start();

    }

    private static void beforeExit(){
        if(isModify){
            boolean b=true;
            do{
                System.out.println("Are you want to save change? Yes[y] No[n]");
                String ch = new Scanner(System.in).nextLine();
                switch (ch){
                    case "y":{
                        try{
                            Database.saveData();
//                            Display.map.put("page",Display.page);
//                            Display.map.put("currentPage",Display.currentPage);
                            new File("recovery.txt").delete();
                            isModify = false;
                            Display.displayMessage("Everything was saved");
                            b = false;
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                    }
                    case "n":{
                        isModify = false;
                        b = false;
                        break;
                    }
                    default:{
                        break;
                    }
                }
            }while (b);

        }
    }

    public void loadingTime(long startTime){
        System.out.print(".");
        long endTime,totalTime;
        endTime = System.currentTimeMillis();
        totalTime = endTime-startTime;
        try {
            Thread.sleep(300);
            System.out.print(".");
            Thread.sleep(200);
            System.out.print(".");
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        notify();
        System.out.println("\nCurrent time loading : "+totalTime);
        startProcess();
    }

}
