package View;

import Controller.StockController;
import Model.Product;
import Ulity.Database;
import Ulity.Validator;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class InsertView {

    public static void insert(){
        Scanner sc = new Scanner(System.in);
        Product product = new Product();
        try{
            product.setId(Database.getLastID()+1);
        }catch (SQLException e){

        }
        String name;
        String price;
        String qty;
        do{
            System.out.println("input name: ");
            name = sc.nextLine();
            name = name.replaceAll("\\s{2,}"," ");
        }while (!Validator.isValidName(name));
        product.setBrand(name.trim());
        do{
            System.out.println("input price: ");
            price = sc.nextLine();
        }while (!Validator.isFloat(price,"Price"));
        product.setPrice(Float.valueOf(price.trim()));
        do{
            System.out.println("input qty: ");
            qty = sc.nextLine();
        }while (!Validator.isNumber(qty,"Qty"));
        product.setQuantity(Integer.valueOf(qty.trim()));

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate localDate = LocalDate.now();
        product.setImportedDate(dtf.format(localDate));

        Display.comfirmBox(product);

        boolean b=true;
        do{
            System.out.println("Are you sure yo want to add this record? Yes[y] No[n]");
            String ch = sc.nextLine();
            switch (ch.toLowerCase().trim()){
                case "y":{
                    if(new StockController().insert(product)){
                        Display.totalPage=(HomeView.products.size()/Display.numberOfPage)+((HomeView.products.size()%Display.numberOfPage<Display.numberOfPage)&&(HomeView.products.size()%Display.numberOfPage!=0)?1:0);
                        Display.currentPage = HomeView.products.size() - (HomeView.products.size()%Display.numberOfPage==0?Display.numberOfPage:HomeView.products.size()%Display.numberOfPage);
                        Display.page=Display.totalPage;
                        HomeView.isModify = true;
                        try {
                            if(HomeView.createFile){
                                    HomeView.fileWriter = new FileWriter("recovery.txt");
                                    HomeView.createFile = false;
                            }
                            HomeView.fileWriter.write(product.getBrand()+"&"+product.getQuantity()+"&"+product.getPrice()+"&"+product.getImportedDate()+"#insert\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Display.displayMessage("Insert success");

                        Display.displayData(HomeView.products);
                    }
                    b = false;
                    break;
                }
                case "n":{
                    Display.displayMessage("You has cancel the insert!");
                    b = false;
                    break;
                }
                default:{
                    Display.displayMessage("Invalid");
                }
            }
        }while (b);
    }

}
