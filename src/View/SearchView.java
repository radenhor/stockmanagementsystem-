package View;

import Controller.StockController;
import Ulity.Validator;

import java.util.Scanner;

public class SearchView {

    public static void searchByName(){
        Scanner sc=new Scanner(System.in);
        String name;

        System.out.println("Input name to search:");
        name = sc.nextLine();


        StockController stockController = new StockController();
        if (stockController.searchByName(name)){
            if (StockController.searchProduct.size()!=0){
                Display.searchResult();
            }else{
                Display.displayMessage("Search not found");
            }

        }else {
            Display.displayMessage("Search not found");
        }
    }
}
