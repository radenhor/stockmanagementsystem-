package View;

import Controller.StockController;
import Ulity.Database;
import Ulity.Validator;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class RemoveView {

    public static void removeByID(){
        Scanner sc=new Scanner(System.in);
        String id;
        do{
            System.out.println("Input ID:");
            id = sc.nextLine();
        }while (!Validator.isNumber(id,"ID"));
        if(new StockController().display(Integer.valueOf(id))==null){
            Display.displayMessage(HomeView.products.size()==0?"Product is empty":"Wrong id");
        }else {
            Display.comfirmBox(new StockController().display(Integer.valueOf(id)));
            boolean b=true;
            do{
                System.out.println("Are you sure yo want to delete this record? Yes[y] No[n]");
                String ch = sc.nextLine();
                switch (ch.toLowerCase().trim()){
                    case "y":{
                        if(new StockController().remove(Integer.valueOf(id))){
                            Display.displayMessage("Delete success");
                            Database.AUTO_ID+=1;
                            if(Display.page>(HomeView.products.size()/Display.numberOfPage)+((HomeView.products.size()%Display.numberOfPage<Display.numberOfPage)&&(HomeView.products.size()%Display.numberOfPage!=0)?1:0)){
                                Display.page--;
                                Display.currentPage-=Display.numberOfPage;
                            }
                            try {
                                if(HomeView.createFile){
                                    HomeView.fileWriter = new FileWriter("recovery.txt");
                                    HomeView.createFile = false;
                                }
                                HomeView.fileWriter.write(id.trim()+"#delete\n");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            HomeView.isModify = true;
                        }
                        b = false;
                        break;
                    }
                    case "n":{
                        Display.displayMessage("You has cancel the Delete!");
                        b = false;
                        break;
                    }
                    default:{
                        Display.displayMessage("Invalid");
                    }
                }
            }while (b);
        }




    }

}
