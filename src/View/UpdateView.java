package View;

import Controller.StockController;
import Model.Product;
import Ulity.Database;
import Ulity.Validator;
import javafx.scene.chart.ValueAxis;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Scanner;

public class UpdateView {

    public static void update(){
        String id;
        do{
            System.out.println("Please Input ID of Product:");
            id = new Scanner(System.in).nextLine();
        }while (!Validator.isNumber(id,"ID"));

        if(new StockController().display(Long.valueOf(id))==null){
            Display.displayMessage("Product Not Found");
        }else {
            Display.comfirmBox(new StockController().display(Long.valueOf(id)));
            System.out.println("What do you want to update?");
            Table table=new Table(1, BorderStyle.UNICODE_DOUBLE_BOX,new ShownBorders(false,false,false,false,false,false,true,true,true,true),false,10);
            table.setColumnWidth(0, 104, 100);
            table.addCell(" 1) All | 2) Name | 3) Quantity     | 4) Unit Price | 5) Back to Menu",new CellStyle(CellStyle.HorizontalAlign.center));
            System.out.println(table.render());

            boolean isContinue = true;
            do{
                Scanner sc=new Scanner(System.in);
                System.out.println("Option (1-5) :");
                String ch = sc.nextLine();

                switch (ch.trim()){

                    case "1":{
                        String name,qty,price;
                        do{
                            System.out.println("input name: ");
                            name = sc.nextLine();
                        }while (!Validator.isValidName(name.trim()));
                        do{
                            System.out.println("input price: ");
                            price = sc.nextLine();
                        }while (!Validator.isFloat(price,"Price"));
                        do{
                            System.out.println("input qty: ");
                            qty = sc.nextLine();
                        }while (!Validator.isNumber(qty,"Qty"));

                        String date = new SimpleDateFormat("yyyy/MM/dd").format(Calendar.getInstance().getTime());
                        HomeView.products.get(StockController.position).setBrand(name.trim());
                        HomeView.products.get(StockController.position).setPrice(Float.valueOf(price.trim()));
                        HomeView.products.get(StockController.position).setQuantity(Integer.valueOf(qty.trim()));
                        HomeView.products.get(StockController.position).setImportedDate(date);
                        //
                        Product p = HomeView.products.get(StockController.position);
                        if (p.getUID() != -1) {
                            Database.updateTemp.add(p);
                        }
                        //
                        try {
                            if(HomeView.createFile){
                                HomeView.fileWriter = new FileWriter("recovery.txt");
                                HomeView.createFile = false;
                            }
                            String data = name.trim()+"&"+qty.trim()+"&"+price.trim()+"&"+date.trim()+"&"+id.trim()+"#updateall\n";
                            HomeView.fileWriter.write(data);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Display.displayMessage("Updated Successfully");
                        HomeView.isModify=true;
                        isContinue=false;
                        break;
                    }
                    case "2":{
                        String name;
                        do{
                            System.out.println("input name: ");
                            name = sc.nextLine();
                        }while (!Validator.isValidName(name));

                        HomeView.products.get(StockController.position).setBrand(name);
                        //
                        Product p = HomeView.products.get(StockController.position);
                        if (p.getUID() != -1) {
                            Database.updateTemp.add(p);
                        }

                        try {
                            if(HomeView.createFile){
                                HomeView.fileWriter = new FileWriter("recovery.txt");
                                HomeView.createFile = false;
                            }
                            String data = name.trim()+"&"+id.trim()+"#updatename\n";
                            HomeView.fileWriter.write(data);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        //
                        Display.displayMessage("Updated Successfully");
                        HomeView.isModify=true;
                        isContinue=false;
                        break;
                    }
                    case "3":{
                        String qty;
                        do{
                            System.out.println("input qty: ");
                            qty = sc.nextLine();
                        }while (!Validator.isNumber(qty,"Qty"));
                        HomeView.products.get(StockController.position).setQuantity(Integer.valueOf(qty.trim()));
                        //
                        Product p = HomeView.products.get(StockController.position);
                        if (p.getUID() != -1) {
                            Database.updateTemp.add(p);
                        }
                        //
                        try {
                            if(HomeView.createFile){
                                HomeView.fileWriter = new FileWriter("recovery.txt");
                                HomeView.createFile = false;
                            }
                            String data = qty.trim()+"&"+id.trim()+"#updateqty\n";
                            HomeView.fileWriter.write(data);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Display.displayMessage("Updated Successfully");
                        HomeView.isModify=true;
                        isContinue=false;
                        break;
                    }
                    case "4":{
                        String price;
                        do{
                            System.out.println("input price: ");
                            price = sc.nextLine();
                        }while (!Validator.isFloat(price,"Price"));

                        HomeView.products.get(StockController.position).setPrice(Float.valueOf(price.trim()));
                        //
                        Product p = HomeView.products.get(StockController.position);
                        if (p.getUID() != -1) {
                            Database.updateTemp.add(p);
                        }
                        //

                        try {
                            if(HomeView.createFile){
                                HomeView.fileWriter = new FileWriter("recovery.txt");
                                HomeView.createFile = false;
                            }
                            String data = price.trim()+"&"+id.trim()+"#updateprice\n";
                            HomeView.fileWriter.write(data);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Display.displayMessage("Updated Successfully");
                        HomeView.isModify=true;
                        isContinue=false;
                        break;
                    }
                    case "5":{
                        isContinue=false;
                        break;
                    }
                    default:{
                        isContinue=true;
                        Display.displayMessage("Invalid");
                        break;
                    }
                }
            }while (isContinue);

        }
    }
}
