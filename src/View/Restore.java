package View;

import Controller.StockController;
import Model.ProductFile;
import Ulity.Validator;
import org.omg.PortableInterceptor.INACTIVE;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Restore {

    public static void restore(){
        File file=new File("File/");

        file.mkdirs();

        if(file.listFiles().length>0){
            System.out.println("************ Please choose file Backup ************");
            for(int i=0;i<file.listFiles().length;i++){
                System.out.println(i+1+") "+file.listFiles()[i].getName());
            }
            Scanner sc=new Scanner(System.in);
            String ch;
            do{
                System.out.print("Choose File Now:");
                ch = sc.nextLine();
            }while (!Validator.isNumber(ch,"Option",file.listFiles().length));
            boolean b=true;
            do{
                System.out.println("Are you sure yo want to restore this record? Yes[y] No[n]");
                String op = sc.nextLine();
                switch (op.toLowerCase().trim()){
                    case "y":{
                        String filePath = (file.listFiles()[Integer.valueOf(ch)-1]).toString();
                        ProductFile.restoreFile(filePath,"181099","postgres","tbproduct");
                        b = false;
                        break;
                    }
                    case "n":{
                        Display.displayMessage("Restore has cancel!");
                        b = false;
                        break;
                    }
                    default:{
                        Display.displayMessage("Invalid");
                    }
                }
            }while (b);

        }else {
            Display.displayMessage("No backup file.");
        }
    }
}
