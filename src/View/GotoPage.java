package View;

import Controller.StockController;
import Ulity.Validator;

import java.util.Scanner;

public class GotoPage {

    public static void goToPage(){
        Scanner sc =new Scanner(System.in);
        String page;
        do{
            System.out.println("Input page number: ");
            page = sc.nextLine();
        }while (!Validator.isNumber(page,"Page"));

        if (new StockController().goToPage(Long.valueOf(page))){
            Display.displayData(HomeView.products);
        }else{
            Display.displayMessage("Wrong page!");
        }

    }

}
