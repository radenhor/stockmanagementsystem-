package CoreApp;

import Model.Product;

import java.util.ArrayList;

public interface StockManagable {

    boolean insertMultiRecord(Product product, long record);

    boolean insertBigData(Product product);

    // Add product to stock
    boolean insert(Product product);

    // Display product to stock
    ArrayList<Product> display();

    // View product from stock
    Product display(long position);

    // Remove product from stock
    boolean remove(long position);

    boolean update(Product product);

    // Search product by name
    boolean searchByName(String keyName);

    // Set number of display row
    void setRowDisplay(long row);

    // Go to specific page to display
    boolean goToPage(long page);
}
