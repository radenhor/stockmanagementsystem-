package CoreApp;

import Model.Product;

import java.util.ArrayList;

public abstract class AbstractStock implements StockManagable{

    @Override
    public boolean insert(Product product) {
        return true;
    }

    @Override
    public boolean insertBigData(Product product) {
        return false;
    }

    @Override
    public boolean insertMultiRecord(Product product, long record) {
        return false;
    }

    @Override
    public ArrayList<Product> display() {
        return null;
    }

    @Override
    public Product display(long position) {
        return null;
    }

    @Override
    public boolean remove(long position) {
        return false;
    }


    @Override
    public boolean searchByName(String keyName) {
        return false;
    }

    @Override
    public void setRowDisplay(long row) {

    }

    @Override
    public boolean goToPage(long page) {
        return false;
    }

    @Override
    public boolean update(Product product) {
        return false;
    }

}
