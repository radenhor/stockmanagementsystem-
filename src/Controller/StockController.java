package Controller;

import CoreApp.AbstractStock;
import Model.Product;
import Ulity.Database;
import View.Display;
import View.HomeView;

import javax.xml.crypto.Data;
import java.sql.SQLException;
import java.util.ArrayList;

public class StockController extends AbstractStock {

    private Product product;

    private ArrayList<Product> products;
    public static int position;

    public static long page=1,totalPage=0,currentPage=0;
    public static ArrayList<Product> searchProduct=new ArrayList<>();

    @Override
    public boolean insertBigData(Product product) {
        for(int i=1;i<=10_000_000;i++){
            HomeView.products.add(new Product(HomeView.products.get(HomeView.products.size()-1).getId()+1,product.getBrand(),product.getPrice(),product.getQuantity(),product.getImportedDate()));
        }
        HomeView.isModify=true;
        return true;
    }

    @Override
    public boolean insertMultiRecord(Product product, long record) {
        for(int i=0;i<record;i++){
            HomeView.products.add(new Product(Database.AUTO_ID=Database.AUTO_ID+1,product.getBrand(),product.getPrice(),product.getQuantity(),product.getImportedDate()));
        }
        return true;
    }

    @Override
    public boolean insert(Product product) {
        if (HomeView.products.size()==0){
            product.setId(1);
            Product p = new Product(product.getId(),product.getBrand(),product.getPrice(),product.getQuantity(),product.getImportedDate());
            HomeView.products.add(p);
            Database.insertTemp.add(p);

        }else{

//            System.out.println(Database.AUTO_ID);
            try {
//                product.setId(Database.getLastID()+1);
                System.out.println(Database.getLastID()+1);
                Product p = new Product(Database.getLastID()+1,product.getBrand(),product.getPrice(),product.getQuantity(),product.getImportedDate());
                HomeView.products.add(p);
                Database.insertTemp.add(p);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

        return true;

    }

    @Override
    public ArrayList<Product> display() {

        return null;
    }

    @Override
    public Product display(long position) {
        products = HomeView.products;
        for (long i=0;i<products.size();i++){
            if (products.get((int)i).getId()==position){
                product = products.get((int)i);
                StockController.position = (int)i;
                return product;
            }
        }
        return null;
    }

    @Override
    public boolean remove(long position) {
        for(long i=0;i<HomeView.products.size();i++){
            if(HomeView.products.get((int) i).getId()==position){
                //
                Product p = HomeView.products.get((int) i);
                for (Product tmp : Database.insertTemp) {
                    if (tmp.getId() == p.getId()) {
                        Database.insertTemp.remove(tmp);
                        break;
                    }
                }
                if (p.getUID() != -1) {
                    Database.deleteTemp.add(p);
                }
                //
                HomeView.products.remove((int) i);
                HomeView.isModify = true;
                return true;
            }
        }
        return false;
    }

    /*
    * Search product in stock
    *
    * apply pagination to search
    * */
    @Override
    public boolean searchByName(String keyName) {

        boolean searchFound = false;
        searchProduct.clear();
        currentPage=0;
        for(long i=0;i<HomeView.products.size();i++){
            if(HomeView.products.get((int)i).getBrand().toLowerCase().contains(keyName.toLowerCase())){
                searchProduct.add(new Product(HomeView.products.get((int)i).getId(),""+HomeView.products.get((int)i).getBrand(), HomeView.products.get((int)i).getPrice(),HomeView.products.get((int)i).getQuantity(),HomeView.products.get((int)i).getImportedDate()));
                searchFound = true;
            }
        }
        if (searchFound){
            page=1;
            HomeView.paginationForSearch=true;
            return true;
        }else{
            searchProduct.clear();
            return false;
        }

    }

    @Override
    public void setRowDisplay(long row) {
        super.setRowDisplay(row);
    }

    @Override
    public boolean goToPage(long page) {
        if(page>Display.totalPage){
            return false;
        }else {
            Display.currentPage=page*Display.numberOfPage-Display.numberOfPage;
            Display.page=page;
            return true;
        }
    }

    @Override
    public boolean update(Product product) {
        return super.update(product);
    }
}
